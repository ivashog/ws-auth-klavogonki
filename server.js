require('dotenv').config();
require('./passport.config');

const path = require('path');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const passport = require('passport');

const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const users = require('./data/users.json');
const texts = require('./data/texts.json');
const Race = require('./race');

server.listen(process.env.APP_PORT || 3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.get('/race', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/race.html'));
});

app.get('/login', function(req, res) {
    res.sendFile(path.join(__dirname, 'views/login.html'));
});

app.post('/login', function(req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.email === userFromReq.email);

    if (userInDB && userInDB.password === userFromReq.password) {
        const token = jwt.sign(userInDB, process.env.JWT_SECRET);
        res.status(200).json({ auth: true, token });
    } else if (!userInDB) {
        res.status(404).json({ auth: false, message: `User with email ${userFromReq.email} is not exist!` });
    } else {
        res.status(401).json({ auth: false, message: `Wrong password for user '${userInDB.password.login}'` });
    }
});

app.get('/text', passport.authenticate('jwt', { session: false }), (req, res) => {
    const randomText = texts[Math.floor(Math.random() * texts.length)];

    res.status(200).json(randomText);
});

const race = new Race(io);

io.on('connection', socket => {
    console.log('>>>> connection');
    let user;

    socket.on('userConnected', ({ token }) => {
        user = jwt.verify(token, process.env.JWT_SECRET);
        if (!user) return false;

        if (!race.isExistUser(user)) {
            if (race.isRunAndPlay) {
                socket.join('queueRoom');
                race.addQueueUsers(user);
            } else if (race.isRunAndWait) {
                socket.join('raceRoom');
                race.addUser(user);
            } else {
                socket.join('raceRoom');
                race.addUser(user);
                race.run();
            }
        } else if (race.isRunAndWait) {
            socket.emit('reconnected', {
                users: race.usersArray,
                text: race.text
            });
        }
    });

    // if (race.isRunAndWait) {
    //     console.log('!', race.timeToStart)
    //     socket.to('raceRoom').emit('runAndWait', {
    //         secondsToStart: race.timeToStart
    //     });
    //     socket.broadcast.to('raceRoom').emit('runAndWait', {
    //         secondsToStart: race.timeToStart
    //     });
    // }

    socket.on('raceText', ({ textId }) => {
        const { text: raceText } = texts.find(text => text.id === +textId);
        race.text = raceText;
    });

    socket.on('disconnect', reason => {
        if (!user) {
            console.log('Some user is disconnected by reason: ', reason);
            return;
        }

        console.log(`user ${user && user.login} is disconnected`);
        if (race.isExistUser(user)) {
            race.deleteUser(user);
            io.sockets.in('raceRoom').emit('userDisconected', {
                username: user.login
            });
            if (!race.isReady) race.endRace();
        } else {
            race.deleteQueueUser(user);
        }
    });
});

