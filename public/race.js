let raceText = {};
let usersOnRace = [];

window.onload = async () => {
    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const countdownWrap = document.querySelector('.countdown-wrapper');
        const raceCountdownElem = document.querySelector('.race-countdown');
        const statisticTable = document.querySelector('.statistic-table');
        const raceTextElem = document.querySelector('.race-text');

        const socket = io.connect('http://localhost:3000');

        socket.emit('userConnected', { token: jwt });

        socket.on('runAndWait', async ({users}) => {
            raceText = await getApiData('/text', jwt);
            socket.emit('raceText', { textId: raceText.id });
            usersOnRace = users;
            renderStatisticRows(statisticTable, usersOnRace);
        });

        socket.on('addUser', async ({users}) => {
            deleteTableRows(statisticTable);
            usersOnRace = users;
            renderStatisticRows(statisticTable, usersOnRace);
        });

        socket.on('reconnected', ({users, text}) => {
            usersOnRace = users;
            raceText = text;
            renderStatisticRows(statisticTable, usersOnRace)
        })

        socket.on('startRaceCountdown', ({secondsToStart}) => {
            raceCountdownElem.textContent = formatWaitingTime(+secondsToStart);
        });

        socket.on('startRace', () => {
            countdownWrap.style.display = 'none';

            fillRaceText(raceTextElem)
        })

        socket.on('endRace', () => {
            socket.emit('raceResults', { users: usersOnRace });
        })

        socket.on('userDisconected', ({ username }) => {
            console.log(`user '${username}' is disconnected`);
        })

    }

};

function formatWaitingTime(seconds) {
    const numMinutes = Math.floor(seconds / 60);
    const strMinutes = numMinutes
        .toString(10)
        .padStart(2, '0');

    const strSeconds = (seconds - numMinutes * 60)
        .toString(10)
        .padStart(2, '0');

    return `${strMinutes}:${strSeconds}`;
}

async function getApiData(url, jwt, method = 'GET') {
    const response = await fetch(url, {
        method,
        headers: {
            'Authorization': 'Bearer ' + jwt
        }
    });

    return await response.json();
}

function renderStatisticRows(table, users) {
    users.forEach((user, idx) => {
        const row = table.insertRow();

        const progress = document.createElement('progress');
        progress.setAttribute('max', raceText.text.length);
        progress.setAttribute('value', user.enteredSymbols);

        const speedValue = user.usedTime && user.enteredSymbols
            ? Math.round(user.enteredSymbols / (user.usedTime / 60)) : 0;

        const errorRateValue = user.enteredSymbols
            ? ((user.errorCount / user.enteredSymbols) * 100).toFixed(2) : 0;

        const rowCellsData = [
            document.createTextNode(String(idx + 1)),
            document.createTextNode(user.login),
            progress,
            document.createTextNode(user.usedTime + ' sec'),
            document.createTextNode(speedValue + ' cpm'),
            document.createTextNode(errorRateValue + '%')
        ];

        rowCellsData.forEach(cellData => {
            let cell = row.insertCell();
            cell.appendChild(cellData);
        })

    });
}

function deleteTableRows(table) {
    for (let i = table.rows.length - 1; i > 0; i--) {
        table.deleteRow(i);
    }
}

function fillRaceText(raceTextElem) {
    const wordsArray = raceText.text.split(' ');

    wordsArray.forEach(word => {
        const wordElem = document.createElement('div');
        wordElem.classList.add('word');

        const lettersArray = word.split('');

        [...lettersArray, ' '].forEach(letter => {
            const letterElem = document.createElement('div');
            letterElem.classList.add('letter');
            letterElem.textContent = letter;

            wordElem.appendChild(letterElem);
        })

        raceTextElem.appendChild(wordElem)
    })


}