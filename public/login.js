window.onload = () => {

    const loginBtn = document.querySelector('#submit-btn');
    const emailInput = document.querySelector('#email-input');
    const passwordInput = document.querySelector('#password-input');

    loginBtn.addEventListener('click', ev => {
        fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: emailInput.value,
                password: passwordInput.value
            })
        }).then(res => {
            res.json().then(body => {
                console.log(body);
                if (body.auth) {
                    localStorage.setItem('jwt', body.token);
                    location.replace('/race');
                } else {
                    console.error('auth failed');
                }
            })
        }).catch(err => {
            console.error('request went wrong');
        })
    });

}