class Race {
    constructor(io) {
        this._io = io;
        this._users = new Map();
        this._queueUsers = new Map();
        this._raceText = null;
        this._isRunAndWait = false;
        this._isRunAndPlay = false;
        this._secondsToStart = 20;
        this._secondsToNextRace = 120;
        this._timerToStart = null;
        this._timerToNextRace = null;
        this._statisticTemplate = {
            usedTime: 0,
            enteredSymbols: 0,
            errorCount: 0
        }
    }

    addUser(user) {
        if (!this.isExistUser(user)) {
            this._users.set(user.email, Object.assign(user, this._statisticTemplate));

            if (this._users.size > 1) {
                this._io.sockets.in('raceRoom').emit('addUser', {
                    users: this.usersArray
                })
            }
        }
    }

    addQueueUsers(user) {
        if (!this._queueUsers.has(user.email)) {
            this._queueUsers.set(user.email, Object.assign(user, this._statisticTemplate));
        }
    }

    deleteUser(user) {
        this._users.delete(user.email);
    }

    deleteQueueUser(user) {
        this._queueUsers.delete(user.email);
    }

    run() {
        if (!this.isReady) return;

        this._isRunAndWait = true;

        this._io.sockets.in('raceRoom').emit('runAndWait', {
            users: this.usersArray
        });

        this._timerToStart = setInterval(() => {
            this._secondsToStart--;
            console.log('> ', this._secondsToStart);

            this._io.sockets.in('raceRoom').emit('startRaceCountdown', {
                secondsToStart: this._secondsToStart
            });

            if (this._secondsToStart === 0) {
                clearInterval(this._timerToStart);

                this._isRunAndWait = false;
                this._isRunAndPlay = true;

                this._io.sockets.in('raceRoom').emit('startRace');

                this._timerToNextRace = setInterval(() => {
                    this._secondsToNextRace--;
                    console.log('< ', this._secondsToNextRace);

                    this._io.sockets.emit('endRaceCountdown', {
                        secondsToEnd: this._secondsToNextRace
                    });

                    if (this._secondsToNextRace === 0) {
                        clearInterval(this._timerToNextRace);

                        this._io.sockets.emit('endRace');

                        this.endRace();
                    } else if (!this.isReady) {
                        clearInterval(this._timerToNextRace);
                    }
                }, 1000);
            } else if (!this.isReady) {
                clearInterval(this._timerToStart);
            }
        }, 1000);
    }

    endRace() {
        clearInterval(this._timerToStart);
        clearInterval(this._timerToNextRace);

        if (this._queueUsers.size) {
            this._queueUsers.forEach((value, key) => {
                this._users.set(key, value);
            });
            this._queueUsers.clear();
        }

        this._raceText = null;
        this._isRunAndWait = false;
        this._isRunAndPlay = false;
        this._secondsToStart = 20;
        this._secondsToNextRace = 120;
        this._timerToStart = null;
        this._timerToNextRace = null;

        this.run();
    }

    isExistUser(user) {
        return this._users.has(user.email);
    }

    get usersArray() {
        return Array.from(this._users.values());
    }

    get isRunAndWait() {
        return this._isRunAndWait;
    }

    get isRunAndPlay() {
        return this._isRunAndPlay;
    }

    get isReady() {
        return !!this._users.size;
    }

    get text() {
        return this.raceText;
    }

    set text(value) {
        this.raceText = value;
    }
}

module.exports = Race;
